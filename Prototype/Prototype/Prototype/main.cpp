// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include "UniformBufferObject.h"
#include "FreeCamera.h"
#include "Model.h"
#include <glm\gtc\matrix_transform.hpp>
#include <random>
#include "Frustum.h"
#include "Skybox.h"
#include "LightManager.h"
#include "AABB.h"

bool running = true, frustumCulling = true;
const float g_FOV = 60.0f;
const float g_RESPAWN_HEIGHT = -2000.0f;
const glm::vec3 g_CAMERA_SPAWN_POS = glm::vec3(-1500.0f, 100.0f, -200.0f);
Shader *blankShader, *phongShader;
Model *block;
FreeCamera *camera;
Material *whiteMaterial;
LightManager *lightManager;

//Skybox Stuff
Skybox *skybox;
Cubemap *skyboxCubemap;
Model *skyboxCube;
Shader *skyboxShader;
std::vector<AABB> boundingBoxes;
AABB cameraBoundingBox;

UniformBufferObject *matricesUBO, *lightsUBO;

GLuint screenWidth = 800, screenHeight = 600;
float aspectRatio = (float)screenWidth / screenHeight;

const float movementSpeed = 50.0f;


struct BuildingCoords
{
	glm::vec3 position;
	glm::vec3 scale;
};

// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
		rt3d::exitFatalError("Unable to initialize SDL");

	// Request an OpenGL 3.0 context.

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)

	// Create window
	window = SDL_CreateWindow("Multiple Lights Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		screenWidth, screenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!window) // Check window was created OK
		rt3d::exitFatalError("Unable to create window");

	context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
	SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void init(void)
{
	camera = new FreeCamera(screenWidth, screenHeight, g_FOV);
	camera->setPosition(g_CAMERA_SPAWN_POS);
	camera->setCullingDistance(1.0f, 25000.0f);
	cameraBoundingBox = AABB(camera->getPosition(), glm::vec3(1.0f));

	blankShader = new Shader("Shaders/blank.vert", "Shaders/blank.frag", "Blank");
	phongShader = new Shader("Shaders/phong.vert", "Shaders/phong.frag", "Phong");

	whiteMaterial = new Material(
		glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
		glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), 
		glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), 
		1.0f);


	block = new Model();
	block->load("Models/cube.dae", false);
	block->setShader(phongShader);
	block->setMaterial(whiteMaterial);
	block->setScale(glm::vec3(2000.0f, 10000.0f, 2000.0f));


	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			glm::vec3 boundingBoxPosition = glm::vec3(-10000.0f + j * 8000.0f, -10000.0f, -10000.0f + i*8000.0f);
			AABB boundingBox(boundingBoxPosition, glm::vec3(2000.0f, 10000.0f, 2000.0f)*2.0f);
			boundingBoxes.push_back(boundingBox);
		}
	}

	const GLchar * matriceNames[] =
	{
		"modelMat",
		"viewMat",
		"modelViewMat",
		"normalMat",
		"projectionMat",
		"mvpMat",
		"eyePos"
	};
	matricesUBO = new UniformBufferObject("MatrixBlock", matriceNames, 7, blankShader, 0, GL_DYNAMIC_DRAW);
	matricesUBO->setUniform("projectionMat", camera->getProjectionMat());

	const GLchar * lightNames[] =
	{
		"numLights",
		"position",
		"direction",
		"ambient",
		"diffuse",
		"specular",
		"attenuation",
		"cosCutoffAngle",
		"secondaryCutoffAngle"
	};
	lightsUBO = new UniformBufferObject("LightBlock", lightNames, 9, phongShader, 1, GL_DYNAMIC_DRAW);

	//Initialise Skybox components
	skyboxCube = new Model();
	skyboxCube->load("Models/cube.obj");
	skyboxCube->setScale(glm::vec3(100.0f, 100.0f, 100.0f));
	skyboxCubemap = new Cubemap();
	skyboxCubemap->load("Skyboxes/Town-skybox/", "Town_bk.bmp", "Town_ft.bmp", "Town_rt.bmp", "Town_lf.bmp", "Town_up.bmp", "Town_dn.bmp");
	skyboxShader = new Shader("Shaders/skyboxCubemap.vert", "Shaders/skyboxCubemap.frag", "Skybox");
	skybox = new Skybox();
	skybox->load(skyboxCubemap);
	skybox->setCube(skyboxCube);
	skybox->setShader(skyboxShader);

	Light *light = new Pointlight(glm::vec4(0.0f), glm::vec4(0.1f, 0.1f, 0.1f, 1.0f), glm::vec4(0.4f, 0.4f, 0.3f, 1.0f), glm::vec4(0.4f, 0.4f, 0.1f, 1.0f));
	AmbientLight *ambient = new AmbientLight(glm::vec4(0.1f, 0.1f, 0.1f, 1.0f));
	lightManager = new LightManager(lightsUBO);
	lightManager->addLight(light);
	lightManager->addLight(ambient);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LESS);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glViewport(0, 0, screenWidth, screenHeight);
}

void update(float deltaTime) 
{
	camera->update();
	if (camera->getPosition().y < g_RESPAWN_HEIGHT)
		camera->setPosition(g_CAMERA_SPAWN_POS);
	cameraBoundingBox.setPosition(camera->getPosition());

	lightManager->update();
}

bool g_jumpPressed = false;


void handleInput()
{
	SDL_Event sdlEvent;
	while (SDL_PollEvent(&sdlEvent))
	{
		if (sdlEvent.type == SDL_KEYUP)
		{
			switch (sdlEvent.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				running = false;
				break;
			case SDLK_c:
				frustumCulling = !frustumCulling;
				break;
			}
		}
		if (sdlEvent.type == SDL_QUIT)
			running = false;
	}

	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if (keys[SDL_SCANCODE_W]) camera->moveForward(movementSpeed);
	if (keys[SDL_SCANCODE_S]) camera->moveForward(-movementSpeed);
	if (keys[SDL_SCANCODE_A]) camera->moveRight(-movementSpeed);
	if (keys[SDL_SCANCODE_D]) camera->moveRight(movementSpeed);
	if (keys[SDL_SCANCODE_R]) camera->moveUp(movementSpeed);
	if (keys[SDL_SCANCODE_F]) camera->moveUp(-movementSpeed);

	if (keys[SDL_SCANCODE_COMMA]) camera->rotateY(-2.0f);
	if (keys[SDL_SCANCODE_PERIOD]) camera->rotateY(2.0f);


	if (keys[SDL_SCANCODE_SPACE]) g_jumpPressed = true;
	else g_jumpPressed = false;

}

bool cameraAtRest()
{
	for (auto boundingBox : boundingBoxes)
	{
		if (cameraBoundingBox.negativeYCollisiion(boundingBox))
		{
			return true;
		}
	}
	return false;
}


void draw(SDL_Window * window)
{	
	// clear the screen
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glm::mat4 viewMat = camera->getViewMat();
	glm::mat4 normalMat = glm::inverse(viewMat);
	glm::vec3 viewPos = camera->getPosition();
	matricesUBO->setUniform("viewMat", viewMat);
	matricesUBO->setUniform("eyePos", viewPos);
	lightManager->updateUniforms();

	skybox->draw(viewMat, matricesUBO);

	Frustum frustum = camera->getFrustum();

	BuildingCoords buildingCoords[100];

	glm::vec3 cubeScale = block->getScale();
	int number = 0;
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			glm::vec3 cubePosition = glm::vec3(-10000.0f + j * 8000.0f, -10000.0f, -10000.0f + i*8000.0f);
			bool draw = false;

			buildingCoords[number].position = cubePosition;
			buildingCoords[number].scale = block->getScale();
			number++;
			if (frustumCulling)			
			{
				for (int k = 0; k < 8 && !draw; k++)
				{
					int xMovement = (k % 2 == 0) ? -1 : 1;

					int yMovement = (int)floor(k / 2.0f);
					yMovement = (yMovement % 2 == 0) ? 1 : -1;

					int zMovement = (int)floor(k / 4.0f);
					zMovement = (zMovement % 2 == 0) ? 1 : -1;

					glm::vec3 pointPosition = glm::vec3(
						cubePosition.x + xMovement*cubeScale.x / 2.0f,
						cubePosition.y + yMovement*cubeScale.y / 2.0f,
						cubePosition.z + zMovement*cubeScale.z / 2.0f
						);

					if (frustum.pointIsInside(pointPosition))
					{
						draw = true;
					}
				}
			}
			else draw = true;

			if (draw)
			{
				block->setPosition(cubePosition);
				block->draw(viewMat, matricesUBO);
			}
		}
	}

	camera->update();


	camera->jump(g_jumpPressed);

	camera->runGravity(cameraAtRest());

	std::cout << '\n' << camera->getPosition().x << "; " << camera->getPosition().y << "; " << camera->getPosition().z;

	SDL_GL_SwapWindow(window); // swap buffers
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
	SDL_Window * hWindow; // window handle
	SDL_GLContext glContext; // OpenGL context handle
	hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << std::endl;
		exit(1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;

	init();


	while (running)
	{	// the event loop
		handleInput();
		update(1.0f);
		draw(hWindow); // call the draw function
	}

	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(hWindow);
	SDL_Quit();
	return 0;
}