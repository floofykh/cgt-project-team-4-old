#include "Skybox.h"
#include <SDL.h>
#include <iostream>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>

using namespace std;
using namespace glm;

Skybox::Skybox() : scale(10.5f)
{
}

void Skybox::draw(glm::mat4 viewMat, UniformBufferObject *ubo)
{
	if (shader != nullptr && cube != nullptr && cubemap != nullptr)
	{
		shader->bindProgram();
		if (ubo != nullptr)
			ubo->bind();
		glDepthMask(GL_FALSE); // make sure writing to update depth test is off
		glCullFace(GL_FRONT); // drawing inside of cube!
		cubemap->bind();
		mat4 modelMat(1.0f);
		modelMat = glm::scale(modelMat, scale);
		glm::mat3 mvRotOnlyMat3 = glm::mat3(viewMat);
		mat4 modelViewMat = mat4(mvRotOnlyMat3) * modelMat;
		mat3 normalMat = transpose(inverse(mat3(modelViewMat)));

		ubo->setUniform("modelMat", modelMat);
		ubo->setUniform("modelViewMat", modelViewMat);
		ubo->setUniform("normalMat", normalMat);

		cube->drawRawData();

		Cubemap::unbind();

		glCullFace(GL_BACK);
		glDepthMask(GL_TRUE);
		UniformBufferObject::unbind();
	}
}

void Skybox::load(string folder, string back, string front, string right, string left, string up, string down)
{
	cubemap = new Cubemap();
	cubemap->load(folder, back, front, right, left, up, down);
}

Skybox::~Skybox()
{
}
