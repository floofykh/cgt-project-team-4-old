#include "Frustum.h"


Frustum::Frustum(glm::vec3 ntl, glm::vec3 ntr, glm::vec3 nbl, glm::vec3 nbr, glm::vec3 ftl, glm::vec3 ftr, glm::vec3 fbl, glm::vec3 fbr)
{
	m_planes[0] = new Plane(ntl, ntr, nbr); //Front Plane
	m_planes[1] = new Plane(fbr, ftr, ftl); //Back Plane
	m_planes[2] = new Plane(ntl, nbr, fbl); //Left Plane
	m_planes[3] = new Plane(ntl, ftl, ftr); //Top Plane
	m_planes[4] = new Plane(ntr, ftr, fbr); //Right Plane
	m_planes[5] = new Plane(nbl, nbr, fbr); //Bottom Plane
}

bool Frustum::pointIsInside(glm::vec3 point)
{
	bool innerSide = false;
	for (int i = 0; i < 6; i++)
	{
		(m_planes[i]->distanceTo(point) >= 0) ? innerSide = true : innerSide = false;
		if (!innerSide)
			return false;
	}
	return true;
}

Frustum::~Frustum()
{
	for (int i = 0; i < 6; i++)
		delete m_planes[i];
}
