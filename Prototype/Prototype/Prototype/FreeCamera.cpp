#include "FreeCamera.h"
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\rotate_vector.hpp>
#include <iostream>

using namespace glm;

FreeCamera::FreeCamera(unsigned int screenWidth, unsigned int screenHeight, float fov)
{
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	m_aspectRatio = (float)screenWidth / screenHeight;
	m_fov = fov;
	m_nearDist = 1.0f;
	m_farDist = 1000.0f;
}

void FreeCamera::moveRight(float distance)
{
	pos = glm::vec3(pos.x + distance*cos(radians(angle)), pos.y, pos.z + distance*sin(radians(angle)));
}

void FreeCamera::moveForward(float distance)
{
	pos = glm::vec3(pos.x + distance*sin(radians(angle)), pos.y, pos.z - distance*cos(radians(angle)));
}

void FreeCamera::moveUp(float distance)
{
	pos.y += distance;
}

void FreeCamera::rotateY(float angle)
{
	this->angle += angle;
	if (this->angle >= 360)
		this->angle -= 360;
	else if (this->angle <= -360)
		this->angle += 360;
}

void FreeCamera::update()
{
	at = glm::vec3(pos.x + 1.0f*sin(radians(angle)), pos.y, pos.z - 1.0f*cos(radians(angle)));

	viewMat = lookAt(pos, at, up);
}

void FreeCamera::jump(bool button)
{
	// Jump
	if (button == true)
	{
		if (pos.y <= 0.0f)
		{
			m_yspeed = 50.0f;
		}
	}
}

void FreeCamera::runGravity(bool atRest) {
	// If the gravity is currently turned on then activate it
	if (atRest == false)
	{
		m_yspeed -= m_gravitationalPull*m_gravitationalPull*m_mass;
		if (m_yspeed < m_MAX_Y_SPEED)
			m_yspeed = m_MAX_Y_SPEED;
	}
	// If the gravtiy is turned off, and the player is moving down, stop them
	else
	{
		if (m_yspeed < 0.0f)
		{
			m_yspeed = 0.0f;
		}
	}

	// Activate the changes
	moveUp(m_yspeed);
}



void FreeCamera::setPosition(glm::vec3 newPosition)
{
	pos = newPosition;
}

void FreeCamera::setCullingDistance(float near, float far)
{
	if (near < far)
	{
		m_nearDist = near;
		m_farDist = far;
	}
}

bool FreeCamera::collision(glm::vec3 pos, glm::vec3 scale ){
	if ( ( pos.x >= pos.x ) && ( pos.x + scale.x >= pos.x ) )
		if ( ( pos.y >= pos.y ) && ( pos.y + scale.y >= pos.y ) )
			if ( ( pos.z >= pos.z ) && ( pos.z + scale.z >= pos.z ) )
				return true;

	return false;
}

Frustum FreeCamera::getFrustum()
{
	glm::vec3 directionZ = glm::normalize(at - pos);
	glm::vec3 directionX = glm::normalize(glm::cross(directionZ, up));

	float nearHalfHeight = tan(glm::radians(m_fov)) * m_nearDist;
	float nearHalfWidth = nearHalfHeight * m_aspectRatio;
	float farHalfHeight = tan(glm::radians(m_fov)) * m_farDist;
	float farHalfWidth = farHalfHeight * m_aspectRatio;

	glm::vec3 ntl = pos + directionZ*m_nearDist + up*nearHalfHeight - directionX*nearHalfWidth;
	glm::vec3 ntr = pos + directionZ*m_nearDist + up*nearHalfHeight + directionX*nearHalfWidth;
	glm::vec3 nbl = pos + directionZ*m_nearDist - up*nearHalfHeight - directionX*nearHalfWidth;
	glm::vec3 nbr = pos + directionZ*m_nearDist - up*nearHalfHeight + directionX*nearHalfWidth;

	glm::vec3 ftl = pos + directionZ*m_farDist + up*farHalfHeight - directionX*farHalfWidth;
	glm::vec3 ftr = pos + directionZ*m_farDist + up*farHalfHeight + directionX*farHalfWidth;
	glm::vec3 fbl = pos + directionZ*m_farDist - up*farHalfHeight - directionX*farHalfWidth;
	glm::vec3 fbr = pos + directionZ*m_farDist - up*farHalfHeight + directionX*farHalfWidth;

	return Frustum(ntl, ntr, nbl, nbr, ftl, ftr, fbl, fbr);
}

glm::mat4 FreeCamera::getProjectionMat()
{
	return glm::perspective(m_fov, (float)m_screenWidth / m_screenHeight, m_nearDist, m_farDist);
}




FreeCamera::~FreeCamera()
{
}
