#pragma once
#include <glm\glm.hpp>
#include "Frustum.h"

class FreeCamera
{
	private:
		glm::vec3 pos = glm::vec3(0.0f, 1.0f, 5.0f);
		glm::vec3 at;
		glm::mat4 viewMat = glm::mat4(1.0f);
		float angle = 0;
		unsigned int m_screenWidth, m_screenHeight;
		float m_fov, m_aspectRatio, m_nearDist, m_farDist;
		const float m_MAX_Y_SPEED = -50.0f;

		float m_yspeed = 0.0f;
		bool m_gravity;
		float m_gravitationalPull = 1.0f;
		float m_mass = 0.5f;

	public:
		glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
		FreeCamera(unsigned int screenWidth, unsigned int screenHeight, float fov);
		glm::mat4 getViewMat(){ return viewMat; }
		glm::vec3 getPosition() { return pos; }
		void moveForward(float distance);
		void moveRight(float distance);
		void moveUp(float distance);
		void rotateY(float angle);
		void setPosition(glm::vec3 position);
		void setCullingDistance(float near, float far);
		void update();
		bool collision(glm::vec3 position, glm::vec3 scale);
		Frustum getFrustum();
		glm::mat4 getProjectionMat();

		void jump(bool button);
		void runGravity(bool atRest);

		~FreeCamera();
};

