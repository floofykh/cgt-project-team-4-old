#include "Plane.h"


Plane::Plane(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3)
{
	glm::vec3 line1 = point1 - point2;
	glm::vec3 line2 = point3 - point2;

	m_normal = glm::cross(line2, line1);

	m_d = -glm::dot(point1, m_normal);
}

float Plane::distanceTo(glm::vec3 point)
{
	return glm::dot(point, m_normal) + m_d;
}