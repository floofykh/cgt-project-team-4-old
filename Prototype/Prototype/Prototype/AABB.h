#pragma once
#include <glm\glm.hpp>

class AABB
{
private:
	glm::vec3 m_position, m_size;
public:
	AABB() : AABB(glm::vec3(0.0f), glm::vec3(0.0f)){}
	AABB(glm::vec3 position, glm::vec3 size) : m_position(position), m_size(size){}
	~AABB(){}

	const bool isCollision(const AABB &other) const
	{
		if (m_position.x < other.m_position.x + other.m_size.x &&
			m_position.x + m_size.x > other.m_position.x &&
			m_position.y < other.m_position.y + other.m_size.y &&
			m_size.y + m_position.y > other.m_position.y)
			return true;

		return false;
	}

	const bool negativeYCollisiion(const AABB &other) const
	{
		if (isCollision(other))
		{
			if (m_position.y - m_size.y / 2.0f <= other.m_position.y + other.m_size.y / 2.0f)
				return true;
		}
		return false;
	}

	void setPosition(const glm::vec3 position) { m_position = position; }
	void setSize(const glm::vec3 size) { m_size = size; }
	void translate(const glm::vec3 delta) { m_position += delta; }
	void scale(const glm::vec3 delta) { m_size += delta; }
};

