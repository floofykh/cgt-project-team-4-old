#pragma once
#include "rt3d.h"
#include <glm/glm.hpp>
#include "Texture.h"
#include "Shader.h"
#include "explosions.h"
#include <vector>

class particles
{
protected:
	int numParticles;
	GLuint vao[1];
	GLuint vbo[2];
	Texture *texture = nullptr;
	GLfloat *positions;
	GLfloat *velocity;
	glm::vec3 minVelocity;
	glm::vec3 maxVelocity;
	unsigned int previousTime = 0;
	Shader *shader;
	std::vector<explosions*> explosionVector;
public:
	particles(const int n);
	~particles();
	void init(Shader *shader);
	int getNumParticles() const { return numParticles; }
	void update();
	void draw(GLfloat pointSize);
	void setTexture(Texture *textureID) { texture = textureID; }
};

