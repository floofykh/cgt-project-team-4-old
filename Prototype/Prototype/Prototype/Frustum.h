#pragma once
#include "Plane.h"
class Frustum
{
private:
	Plane *m_planes[6];
public:
	Frustum(glm::vec3 ntl, glm::vec3 ntr, glm::vec3 nbl, glm::vec3 nbr, glm::vec3 ftl, glm::vec3 ftr, glm::vec3 fbl, glm::vec3 fbr);
	bool pointIsInside(glm::vec3 point);
	~Frustum();
};

