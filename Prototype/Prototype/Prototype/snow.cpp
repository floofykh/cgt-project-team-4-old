#include "snow.h"
#include <iostream>
using namespace std;



snow::snow(const int n) : particles(n)
{
	if (numParticles <= 0) //Invalid Input
		return;

	//Set minimum and maximum velocities
	minVelocity = glm::vec3(0, 10.0, 0);
	maxVelocity = glm::vec3(0, 20.0, 0);
}

snow::~snow(){}
