#pragma once

#include <glm\glm.hpp>

class Plane
{
private:
	glm::vec3 m_normal;
	float m_d;
public:
	Plane(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3);
	float distanceTo(glm::vec3 point);
	~Plane(){}
};

