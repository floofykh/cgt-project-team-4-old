#pragma once
#include "Shader.h"
#include "Model.h"
#include <string>
#include <glm\glm.hpp>
#include "Cubemap.h"
class Skybox
{
private:
	glm::vec3 scale;
	Shader *shader = nullptr;
	Model *cube = nullptr;
	Cubemap *cubemap = nullptr;
	GLuint id;
public:
	Skybox();
	void load(std::string folder, std::string back, std::string front, std::string right, std::string left, std::string up, std::string down);
	void load(Cubemap *cubemap) { this->cubemap = cubemap; }
	void draw(glm::mat4 viewMat, UniformBufferObject *ubo);
	void setCube(Model * model) { this->cube = model; }
	void setShader(Shader *shader) { this->shader = shader; }
	~Skybox();
};

