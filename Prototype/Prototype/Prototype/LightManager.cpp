#include "LightManager.h"
#include "UniformBufferObject.h"
#include <SDL.h>

LightManager::LightManager(UniformBufferObject *lightsUBO) : lightsUBO(lightsUBO)
{
	lights = new std::vector<Light*>();
}

void LightManager::addLight(Light *light)
{
	if (lights->size() < MAX_LIGHTS && lightsUBO != nullptr)
	{
		lights->push_back(light);
		lightsUBO->bind();
		lightsUBO->setUniform("numLights", lights->size());
		UniformBufferObject::unbind();
		light->setUniforms(lightsUBO, lights->size()-1);
	}
}

void LightManager::addLight(AmbientLight *light)
{
	ambientLight = light;
	lightsUBO->bind();
	lightsUBO->setUniform("numLights", lights->size());
	UniformBufferObject::unbind();
	ambientLight->setUniforms(lightsUBO, 0);
}

void LightManager::updateUniforms()
{
	for (unsigned int i = 0; i < lights->size(); i++)
	{
		lights->at(i)->setUniforms(lightsUBO, i);
	}
	if (ambientLight != nullptr)
		ambientLight->setUniforms(lightsUBO, 0);
}

void LightManager::flipSwitches()
{
	for (unsigned int i = 0; i < lights->size(); i++)
	{
			lights->at(i)->turnOnOff();
	}
	if (ambientLight != nullptr)
		ambientLight->turnOnOff();
}

void LightManager::flipSwitches(std::string tag)
{
	for (unsigned int i = 0; i < lights->size(); i++)
	{
		if (lights->at(i)->getTag() == tag)
			lights->at(i)->turnOnOff();
	}
	if (ambientLight != nullptr)
		if (ambientLight->getTag() == tag)
			ambientLight->turnOnOff();
}

void LightManager::update()
{
	for each (Light *light in *lights)
	{
		if (light->tag == "Pointlight")
		{
			light->position.y = (sin(SDL_GetTicks()/1000.0f)+1)*60 + 30;
		}
	}
}

void LightManager::draw(glm::mat4 viewMat, UniformBufferObject *ubo)
{
	for (unsigned int i = 0; i < lights->size(); i++)
	{
		lights->at(i)->setUniforms(lightsUBO, i);
		lights->at(i)->draw(viewMat, ubo);
	}
}

LightManager::~LightManager()
{
}
