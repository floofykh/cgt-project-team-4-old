// cubeMap fragment shader
#version 330

precision highp float;

layout (std140) uniform MatrixBlock
{
	mat4 modelMat;
	mat4 viewMat;
	mat4 modelViewMat;
	mat4 projectionMat;
	mat3 normalMat;
};

layout (location = 0) out vec4 outColour;
in vec3 cubeTexCoord;

uniform samplerCube cubeMap;
 
void main(void) 
{   
	outColour = texture(cubeMap, cubeTexCoord);
}