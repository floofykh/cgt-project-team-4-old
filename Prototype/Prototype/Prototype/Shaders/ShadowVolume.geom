#version 330

const int maxLights = 102;
const float SHADOW_OFFSET = 0.1f;

layout (shared) uniform LightBlock
{
	int numLights;
	vec4 position[maxLights];
	vec4 direction[maxLights];
	vec4 ambient;
	vec4 diffuse[maxLights];
	vec4 specular[maxLights];
	vec3 attenuation[maxLights];
	float cosCutoffAngle[maxLights];
	float secondaryCutoffAngle[maxLights];
};

layout (shared) uniform MatrixBlock
{
	mat4 modelMat;
	mat4 viewMat;
	mat4 modelViewMat;
	mat4 projectionMat;
	mat4 mvpMat;
	mat3 normalMat;
	vec3 eyePos;
};

layout (triangles_adjacency) in;
layout (triangle_strip, max_vertices = 18) out;

in Vectors
{
	vec3 vertexWorldPosition;
} in_Vectors[];

vec3 worldPositions[6];

vec3 innerEdges[3], outerEdges[6];
void calculateEdges();
vec3 calculateAveragePosition(vec3 position1, vec3 position2, vec3 position3);
void extrudeEdgeToInfinity(vec3 startPos, vec3 endPos, vec3 origin);
vec4 extrudedToInfinity(vec3 position, vec3 origin);
void emitTriangle(vec4 vert1, vec4 vert2, vec4 vert3);

void main()
{
	//Reverse order of vertices as they do much the default winding order.
	for(int i=0; i<6; i++)
	{
		worldPositions[i] = in_Vectors[5-i].vertexWorldPosition;
	}

	calculateEdges();

	int i = 0;

	vec3 mainTriangleNormal = cross(innerEdges[0], -innerEdges[2]);
	vec3 averagePosition = calculateAveragePosition(worldPositions[0], worldPositions[2], worldPositions[4]);
	vec3 lightDirectionToMainTri = normalize(position[i].xyz - averagePosition);

	//Check if the triangle faces the light, this stops us from generating two faces, one for each adjacent triangle bordering a silhouette edge. 
	if(dot(mainTriangleNormal, lightDirectionToMainTri) > 0)
	{
		//We now start checking each adjacent face, and if they do not face the light, then the edge bordering the triangles is a silhouette edge
		//Face 1
		vec3 normal = cross(innerEdges[0], outerEdges[0]);
		averagePosition = calculateAveragePosition(worldPositions[0], worldPositions[1], worldPositions[2]);
		vec3 lightDirection = normalize(position[i].xyz - averagePosition);

		if(dot(normal, lightDirection) <= 0)
		{
			//We extrude our edge to infinity along the direction from the light source
			extrudeEdgeToInfinity(worldPositions[0], worldPositions[2], position[i].xyz);
		}

		//Face 2
		normal = cross(innerEdges[1], outerEdges[2]);
		averagePosition = calculateAveragePosition(worldPositions[2], worldPositions[3], worldPositions[4]);
		lightDirection = normalize(position[i].xyz - averagePosition);

		if(dot(normal, lightDirection) <= 0)
		{
			//We extrude our edge to infinity along the direction from the light source
			extrudeEdgeToInfinity(worldPositions[2], worldPositions[4], position[i].xyz);
		}

		//Face 3
		normal = cross(innerEdges[2], outerEdges[4]);
		averagePosition = calculateAveragePosition(worldPositions[4], worldPositions[5], worldPositions[0]);
		lightDirection = normalize(position[i].xyz - averagePosition);

		if(dot(normal, lightDirection) <= 0)
		{
			//We extrude our edge to infinity along the direction from the light source
			extrudeEdgeToInfinity(worldPositions[4], worldPositions[0], position[i].xyz);
		}

		/*//Now we close the generated volume by creating the front and back caps for it.
		//Front cap
		vec3 direction1 = normalize(worldPositions[0] - position[i].xyz);
		vec3 direction2 = normalize(worldPositions[2] - position[i].xyz);
		vec3 direction3 = normalize(worldPositions[4] - position[i].xyz);
		emitTriangle(	vec4(worldPositions[0] + direction1*SHADOW_OFFSET, 1.0f), 
						vec4(worldPositions[2] + direction2*SHADOW_OFFSET, 1.0f), 
						vec4(worldPositions[4] + direction3*SHADOW_OFFSET, 1.0f)
					);
		EndPrimitive();

		//Back cap
		vec4 position1 = extrudedToInfinity(worldPositions[0], position[i].xyz);
		vec4 position2 = extrudedToInfinity(worldPositions[4], position[i].xyz);
		vec4 position3 = extrudedToInfinity(worldPositions[2], position[i].xyz);
		emitTriangle(position1, position2, position3);
		EndPrimitive();*/

	}
}

void calculateEdges()
{
	//Inner edges
	innerEdges[0] = worldPositions[2] - worldPositions[0];
	innerEdges[1] = worldPositions[5] - worldPositions[2];
	innerEdges[2] = worldPositions[0] - worldPositions[5];

	outerEdges[0] = worldPositions[1] - worldPositions[0];
	outerEdges[1] = worldPositions[2] - worldPositions[1];
	outerEdges[2] = worldPositions[3] - worldPositions[2];
	outerEdges[3] = worldPositions[4] - worldPositions[3];
	outerEdges[4] = worldPositions[5] - worldPositions[4];
	outerEdges[5] = worldPositions[0] - worldPositions[5];
}

vec3 calculateAveragePosition(vec3 position1, vec3 position2, vec3 position3)
{
	return (position1 + position2 + position3) / 3.0f;
}

void extrudeEdgeToInfinity(vec3 startPos, vec3 endPos, vec3 origin)
{
	vec3 directionStart = normalize(startPos - origin);
	vec3 directionEnd = normalize(endPos - origin);

	gl_Position = projectionMat * viewMat * vec4(startPos + directionStart * SHADOW_OFFSET, 1.0f);
	EmitVertex();
	gl_Position = projectionMat * viewMat * vec4(endPos + directionEnd * SHADOW_OFFSET, 1.0f);
	EmitVertex();
	gl_Position = projectionMat * viewMat * vec4(directionEnd, 0.0f);
	EmitVertex();
	gl_Position = projectionMat * viewMat * vec4(directionStart, 0.0f);
	EmitVertex();

	EndPrimitive();
}

vec4 extrudedToInfinity(vec3 position, vec3 origin)
{
	vec3 direction = position - origin;
	return vec4(normalize(direction), 0.0f);
}

void emitTriangle(vec4 vert1, vec4 vert2, vec4 vert3)
{
	gl_Position = projectionMat * viewMat * vert1;
	EmitVertex();
	gl_Position = projectionMat * viewMat * vert2;
	EmitVertex();
	gl_Position = projectionMat * viewMat * vert3;
	EmitVertex();
}
