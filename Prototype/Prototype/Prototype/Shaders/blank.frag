#version 330

precision highp float;

layout (location = 0) out vec4 outColour;
 
void main(void) 
{   
	outColour = vec4(0.6f, 0.6f, 0.6f, 1.0f);
}