// Vertex Shader � file "particles.vert"
#version 330

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_Color;
out vec3 ex_Color;
out float height;

layout(shared) uniform MatrixBlock
{
	 mat4 modelMat;
	 mat4 viewMat;
	 mat4 modelviewMat;
	 mat4 projectionMat;
	 mat3 normalMat;
	 vec3 eyePos;
};

void main(void)
{
	ex_Color = in_Color; 
	vec4 originalPos = vec4(in_Position, 1.0); 
	gl_Position = projectionMat * modelviewMat * (originalPos);
	height = gl_Position.y;
}
