#include "ShaderManager.h"

ShaderManager *ShaderManager::m_instance = nullptr;

ShaderManager* ShaderManager::getInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new ShaderManager();
	}

	return m_instance;
}

bool ShaderManager::addShader(V_ShaderProgram *newProgram)
{
	if (m_shaderMap.find(newProgram->getName()) == m_shaderMap.end())
	{
		m_shaderMap[newProgram->getName()] = newProgram;
		return true;
	}

	return false;
}

bool ShaderManager::removeShader(std::string name)
{
	std::map<std::string, V_ShaderProgram*>::iterator iter = m_shaderMap.find(name);
	if (iter != m_shaderMap.end())
	{
		m_shaderMap.erase(iter);
		return true;
	}

	return false;
}

V_ShaderProgram* ShaderManager::getShader(std::string name)
{
	if (m_shaderMap.find(name) != m_shaderMap.end())
	{
		return m_shaderMap[name];
	}

	return nullptr;
}

void ShaderManager::setUniforms(std::string name, float val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, int val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, bool val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, glm::vec2 val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, glm::vec3 val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, glm::vec4 val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, glm::mat2 val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, glm::mat3 val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

void ShaderManager::setUniforms(std::string name, glm::mat4 val)
{
	for (auto pair : m_shaderMap)
	{
		pair.second->setShaderUniform(name, val);
	}
}

