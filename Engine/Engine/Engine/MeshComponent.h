#ifndef MeshComponent_h
#define MeshComponent_h

#include "V_ActorComponent.h"
#include "V_ShaderProgram.h"

#include <glm\glm.hpp>
#include <vector>


class MeshComponent : virtual public V_ActorComponent {
private:
	std::string m_file;
	V_ShaderProgram *m_program = nullptr;
 public:
	 MeshComponent(std::string file, CGTEngine *engine);

	 bool intialise();

	 void draw();

	 void update();

	 void destroy();

	 std::string getName(){ return "Mesh"; }

	 void setOwner(Actor *actor) { m_actor = actor; }
	 void setShaderProgram(V_ShaderProgram *program) { m_program = program; }
};

#endif // MeshComponent_h
