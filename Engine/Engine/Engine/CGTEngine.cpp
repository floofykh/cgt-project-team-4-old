#include "CGTEngine.h"

#include "Actor.h"
#include "EventManager.h"
#include "MeshManager.h"
#include "V_GameLayer.h"
#include "V_ViewLayer.h"
#include "V_ApplicationLayer.h"
#include "WindowsApplication.h"
#include "OpenGLView.h"
#include "GLShaderProgram.h"
#include "ShaderManager.h"
#include "Camera.h"
#include "PizzaMan.h"

#include <iostream>
    /** 
     *  Main class which makes up the entire engine. Holds all the necessary objects and makes them run. 
     */




	/** 
     *  Initialises all the member variables and resources for the game.
     */
bool CGTEngine::initialise()
{
	/*m_applicationLayer = new WindowsApplication(this);
	if (!m_applicationLayer->initialise())
		return false;
		*/
	m_eventManager = new EventManager();

	m_shaderManager = ShaderManager::getInstance();
	
	m_meshManager = MeshManager::getInstance();
	m_meshManager->setEngine(this);

	m_viewLayer = new OpenGLView(this);
	if (!m_viewLayer->initialise())
		return false;

	GLShaderProgram * phongShader = new GLShaderProgram(this, "Shaders/phong.vert", "Shaders/phong.frag", "Phong");
	phongShader->intitialise();
	m_shaderManager->addShader(phongShader);

	glm::mat4 projectionMat = glm::perspective(60.0f, 800.0f / 600.0f, 1.0f, 10000.0f);
	setMatrix("projectionMat", projectionMat);

	m_camera = new Camera();
	m_camera->setPosition(glm::vec3(0.0f, 0.0f, -10.0f));
	setMatrix("viewMat", m_camera->getViewMatrix());
	setMatrix("modelMat", glm::mat4(1.0f));

	m_gameLayer = new PizzaMan(this);
	m_gameLayer->initialise();

	return true;
}

void CGTEngine::update()
{
	m_applicationLayer->update();
	m_viewLayer->update();
	m_meshManager->update();
	m_gameLayer->update();
	if (m_camera != nullptr)
		m_camera->update();
	setMatrix("viewMat", m_camera->getViewMatrix());
}

void CGTEngine::render()
{
	m_viewLayer->preRender();
	m_viewLayer->render();
	m_meshManager->drawMeshes();
	m_viewLayer->postRender();
}

	/** 
     *  Saves game to the specified file. Consider using XML instead. 
     */
bool CGTEngine::save(std::string file)
{
    return false;
}

bool CGTEngine::load(std::string file)
{
    return false;
}

void CGTEngine::setMatrix(std::string matrixName, glm::mat4 mat)
{
	mat4Map[matrixName] = mat;
	m_shaderManager->setUniforms(matrixName, mat);

	if (matrixName == "modelMat")
	{
		setMatrix("modelViewMat", mat4Map["viewMat"] * mat);
	}
}

void CGTEngine::setMatrix(std::string matrixName, glm::mat3 mat)
{
	mat3Map[matrixName] = mat;
	m_shaderManager->setUniforms(matrixName, mat);
}
