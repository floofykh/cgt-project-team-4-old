#ifndef V_GameLayer_h
#define V_GameLayer_h

class CGTEngine; //All subclasses of V_GameLayer must #include "CGTEngine.h" in their .cpp file.


    /** @author Kyle Hewitt
     */
class V_GameLayer {

 public:
	 V_GameLayer(CGTEngine *engine) { m_engine = engine; }

    virtual bool initialise()  = 0;


    /** 
     *  Time in seconds since the last game update.
     *  @author Kyle Hewitt
     */
    virtual void update()  = 0;

public:
    // virtual destructor for interface 
    virtual ~V_GameLayer() { }


 protected:

    /**
     * @element-type CGTEngine
     */
    CGTEngine *m_engine;
};

#endif // V_GameLayer_h
