#ifndef V_ActorComponent_h
#define V_ActorComponent_h

#include <string>

class Actor; //All subclasses of V_ActorComponent must #include "Actor.h" in their .cpp file.
class CGTEngine;
    /** 
     *  Interface describing what an actor component must implement. Actor components are the things that make up an actor's behaviour and look. 
     */

class V_ActorComponent {
	friend class Actor;
 public:

    virtual void update()  = 0;

    virtual bool intialise()  = 0;

    virtual void destroy()  = 0;

	virtual std::string getName() = 0;

public:
    // virtual destructor for interface 
    virtual ~V_ActorComponent() { }


 protected:

    Actor *m_actor;
	CGTEngine *m_engine;
};

#endif // V_ActorComponent_h
