#include "OpenGLView.h"

#include "MeshComponent.h"
#include "CGTEngine.h"
#include "V_ApplicationLayer.h"
#include "WindowsApplication.h"
#include "rt3d.h"
#include "rt3dObjLoader.h"

#include <assimp\scene.h>
#include <assimp\mesh.h>
#include <assimp\postprocess.h>
#include <glm\gtc\matrix_transform.hpp>
#include <vector>
#include <GL\wglew.h>

bool OpenGLView::initialise()
{
	importer = new Assimp::Importer();

	HGLRC hrc;
	HDC deviceContext = dynamic_cast<WindowsApplication*>(m_engine->m_applicationLayer)->getDeviceContext();
	HINSTANCE instance = dynamic_cast<WindowsApplication*>(m_engine->m_applicationLayer)->getInstance();
	HWND window = dynamic_cast<WindowsApplication*>(m_engine->m_applicationLayer)->getWindow();

	if (!initGlew(instance))
		return false;

	bool error = false;
	PIXELFORMATDESCRIPTOR pixelFormatDescriptor;

	if ((WGLEW_ARB_create_context) && (WGLEW_ARB_pixel_format))
	{
		const int pixelFormatAttribList[] =
		{
			WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
			WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
			WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
			WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
			WGL_COLOR_BITS_ARB, 32,
			WGL_DEPTH_BITS_ARB, 24,
			WGL_STENCIL_BITS_ARB, 8,
			0 // End of Attributes List
		};

		const int contextAttribs[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 3,
			WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
			0, // End of Attrubutes List
		};

		int pixelFormat, numFormats;
		wglChoosePixelFormatARB(deviceContext, pixelFormatAttribList, NULL, 1, &pixelFormat, (UINT*)&numFormats);

		if (!SetPixelFormat(deviceContext, pixelFormat, &pixelFormatDescriptor))
			return false;

		hrc = wglCreateContextAttribsARB(deviceContext, 0, contextAttribs);

		// If everything went okay...
		if (hrc)
			wglMakeCurrent(deviceContext, hrc);
		else
			error = true;
	}

	if (error)
	{
		char errorMessage[255], errorTitle[255];
		printf(errorMessage, "OpenGL %d.%d is not supported! Please Download Latest GPU Drivers!", 3, 3);
		printf(errorTitle, "OpenGL %d.%d Not Supported", 3, 3);
		MessageBox(window, errorMessage, errorTitle, MB_ICONINFORMATION);
		return false;
	}

	//glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_CLAMP);
	//glCullFace(GL_FRONT);
	glClearColor(1.0f, 0.0f, 0.0f, 1.0f);

	checkGLErr();

	return true;
}

LRESULT CALLBACK messageHandlerOpenGL(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT result = 0;
	PAINTSTRUCT paintStruct;

	switch (message)
	{
	case WM_PAINT:
	{
		BeginPaint(window, &paintStruct);
		EndPaint(window, &paintStruct);
	} break;

	default:
	{
		result = DefWindowProc(window, message, wParam, lParam);
	}
	}

	return result;
}

bool OpenGLView::initGlew(HINSTANCE instance)
{
	WNDCLASSEX windowClass;

	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS;
	windowClass.lpfnWndProc = (WNDPROC)messageHandlerOpenGL;
	windowClass.cbClsExtra = 0; windowClass.cbWndExtra = 0;
	windowClass.hInstance = instance;
	windowClass.hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDI_APPLICATION));
	windowClass.hIconSm = LoadIcon(instance, MAKEINTRESOURCE(IDI_APPLICATION));
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = (HBRUSH)(COLOR_MENUBAR + 1);
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = "SimpleOpenGLClass";

	RegisterClassEx(&windowClass);

	HWND fakeWindow = CreateWindow(
		"SimpleOpenGLClass",
		"Fake",
		WS_OVERLAPPEDWINDOW | WS_MAXIMIZE | WS_CLIPCHILDREN,
		0, 0,
		CW_USEDEFAULT, CW_USEDEFAULT,
		NULL,
		NULL,
		instance,
		NULL
		);

	HDC deviceContext = GetDC(fakeWindow);

	PIXELFORMATDESCRIPTOR pixelFormatDescriptor;
	//memset( &pixelFormatDescriptor, 1, sizeof(PIXELFORMATDESCRIPTOR) );
	pixelFormatDescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixelFormatDescriptor.nVersion = 1;
	pixelFormatDescriptor.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits = 32;
	pixelFormatDescriptor.cDepthBits = 32;
	pixelFormatDescriptor.iLayerType = PFD_MAIN_PLANE;

	int pixelFormat = ChoosePixelFormat(deviceContext, &pixelFormatDescriptor);
	if (pixelFormat == 0)
		return false;

	if (!SetPixelFormat(deviceContext, pixelFormat, &pixelFormatDescriptor))
		return false;

	// Create a false, old-style context (OpenGL 2.1 and before)
	HGLRC hRCFake = wglCreateContext(deviceContext);
	wglMakeCurrent(deviceContext, hRCFake);

	bool result = true;

	if (glewInit() != GLEW_OK)
	{
		MessageBox(dynamic_cast<WindowsApplication*>(m_engine->m_applicationLayer)->getWindow(), "Couldn't Initialize GLEW!", "Fatal Error", MB_ICONERROR);
		result = false;
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hRCFake);
	DestroyWindow(fakeWindow);

	return result;
}

void OpenGLView::preRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	checkGLErr();
}

void OpenGLView::render()
{
}

void OpenGLView::postRender()
{
	m_engine->m_applicationLayer->swapBuffers();
	checkGLErr();
}

void OpenGLView::update()
{
}

void OpenGLView::assimpImport(std::string file)
{
	Assimp::Importer *importer = new Assimp::Importer();
	const aiScene *scene = importer->ReadFile(file, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType | aiProcess_GenSmoothNormals);

	if (scene == NULL)
		return;

	std::vector<GLuint> indices;
	const aiMesh* mesh = scene->mMeshes[0];
	Mesh newMesh = Mesh();

	for (unsigned int faceIndex = 0; faceIndex < mesh->mNumFaces; faceIndex++)
	{
		aiFace face = mesh->mFaces[faceIndex];

		assert(face.mNumIndices == 3);

		for (int index = 0; index < 3; index++)
			indices.push_back(face.mIndices[index]);
	}

	newMesh.numIndices = indices.size();

	// generate and set up a VAO for the mesh
	glGenVertexArrays(1, &newMesh.vao);
	glBindVertexArray(newMesh.vao);

	GLuint VBO;

	if (mesh->HasPositions())
	{
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, mesh->mNumVertices * 3 * sizeof(GLfloat), mesh->mVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
	}

	// VBO for normal data
	if (mesh->HasNormals()) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, mesh->mNumVertices * 3 * sizeof(GLfloat), mesh->mNormals, GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);
	}

	// VBO for tex-coord data
	if (mesh->HasTextureCoords(0)) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, mesh->mNumVertices * 2 * sizeof(GLfloat), mesh->mTextureCoords[0], GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);
	}

	if (mesh->HasFaces()) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, newMesh.numIndices * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);
	}

	// unbind vertex array
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return;
}

void OpenGLView::rt3dImport(std::string file)
{
	std::vector<GLfloat> cubeVerts;
	std::vector<GLfloat> cubeNorms;
	std::vector<GLfloat> cubeTex_coords;
	std::vector<GLuint> cubeIndices;
	rt3d::loadObj(file.c_str(), cubeVerts, cubeNorms, cubeTex_coords, cubeIndices);
	GLuint indexCount = cubeIndices.size();
	GLuint vertexCount = cubeVerts.size();
	GLuint vao;

	// generate and set up a VAO for the mesh
	glGenVertexArrays(1, &vao);
	checkGLErr();
	glBindVertexArray(vao);
	checkGLErr();


	if (cubeVerts.size() == 0) {
		return;
	}

	// generate and set up the VBOs for the data
	GLuint VBO;
	glGenBuffers(1, &VBO);
	checkGLErr();

	// VBO for vertex data
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, vertexCount*sizeof(GLfloat), cubeVerts.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	checkGLErr();

	// VBO for normal data
	if (cubeNorms.size() != 0) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNorms.size()*sizeof(GLfloat), cubeNorms.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);
		checkGLErr();
	}

	// VBO for tex-coord data
	if (cubeTex_coords.size() != 0) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, cubeTex_coords.size()*sizeof(GLfloat), cubeTex_coords.data(), GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);
		checkGLErr();
	}

	if (cubeIndices.size() != 0) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(GLuint), cubeIndices.data(), GL_STATIC_DRAW);
		checkGLErr();
	}
	// unbind vertex array
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	checkGLErr();

	Mesh newMesh = Mesh();
	newMesh.numIndices = indexCount;
	newMesh.vao = vao;
	m_meshes[file] = newMesh;
}

void OpenGLView::addMesh(bool isTransparent, const std::string file)
{
	//rt3dImport(file);
	assimpImport(file);
}


void OpenGLView::removeMesh(const std::string file)
{
	m_meshes.erase(m_meshes.find(file));
}


void OpenGLView::drawMesh(const std::string file)
{
	if (m_meshes.find(file) != m_meshes.end())
	{
		glBindVertexArray(m_meshes[file].vao);
		checkGLErr();
		glDrawElements(GL_TRIANGLES, m_meshes[file].numIndices, GL_UNSIGNED_INT, NULL);
		checkGLErr();
		glBindVertexArray(0);
		checkGLErr();
	}
}

void checkOpenGLErr(const char* file, int line)
{
	GLint error;

	do
	{
		error = glGetError();

		if (error != GL_NO_ERROR)
			std::cout << "OpenGL error - " << file << " : " << line << " : ";

		switch (error)
		{
		case (GL_INVALID_ENUM) :
			std::cout << "Invalid Enum" << std::endl;
			break;
		case (GL_INVALID_VALUE) :
			std::cout << "Invalid Value" << std::endl;
			break;
		case (GL_INVALID_OPERATION) :
			std::cout << "Invalid Operation" << std::endl;
			break;
		case (GL_STACK_OVERFLOW) :
			std::cout << "Stack Overflow" << std::endl;
			break;
		case (GL_STACK_UNDERFLOW) :
			std::cout << "Stack Underflow" << std::endl;
			break;
		case (GL_OUT_OF_MEMORY) :
			std::cout << "Out of Memory" << std::endl;
			break;
		case (GL_INVALID_FRAMEBUFFER_OPERATION) :
			std::cout << "Invalid Framebuffer Operation" << std::endl;
			break;
		case (GL_TABLE_TOO_LARGE) :
			std::cout << "Table Too Large" << std::endl;
			break;
		}
	} while (error != GL_NO_ERROR);
}

OpenGLView::~OpenGLView()
{
	//delete importer;
}
