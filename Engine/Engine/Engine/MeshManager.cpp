#include "MeshManager.h"

#include "CGTEngine.h"
#include "V_ViewLayer.h"

#include <algorithm>

MeshManager *MeshManager::s_instance = nullptr;

MeshManager *MeshManager::getInstance()
{
	//If the instance has been initialised return it, otherwise create it and then return it. 

	if (s_instance == nullptr)
		s_instance = new MeshManager();

	return s_instance;
}

MeshComponent* MeshManager::createMesh(std::string file)
{
	MeshComponent *newMesh = new MeshComponent(file, m_engine);

	if (m_meshes.find(file) == m_meshes.end())
	{
		m_engine->m_viewLayer->addMesh(false, file);
		MeshData newMeshData = MeshData();
		newMeshData.components.push_back(newMesh);
		newMeshData.inGPUMemory = true;
		newMeshData.timeSinceLastUse_ms = 0.0f;
		newMeshData.usagePerFrame = 0.0f;

		m_meshes[file] = newMeshData;
	}
	else
	{
		m_meshes[file].components.push_back(newMesh);
	}

	return newMesh;
}

void MeshManager::drawMeshes()
{
	//Iterate through all the meshes and draw if they are in the GPU memory
	for each(std::pair<std::string, MeshData> pair in m_meshes)
	{
		for each(MeshComponent* mesh in pair.second.components)
		{
			mesh->draw();
		}
	}
}

void MeshManager::update()
{
	//Iterate through all the meshes and do stuff...
	for each(std::pair<std::string, MeshData> pair in m_meshes)
	{

	}
}


MeshManager::~MeshManager()
{
}
