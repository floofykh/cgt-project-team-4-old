#include "MeshComponent.h"

#include "CGTEngine.h"
#include "V_ViewLayer.h"
#include "Actor.h"
#include "TransformComponent.h"

MeshComponent::MeshComponent(std::string file, CGTEngine *engine) : m_file(file)
{
	m_engine = engine;
}

bool MeshComponent::intialise()
{
	return true;
}

void MeshComponent::draw()
{
	if (m_actor != nullptr && m_program != nullptr)
	{
		TransformComponent *transform = (TransformComponent*)m_actor->getComponent("Transform");

		m_program->bind();
		m_engine->setMatrix("modelMat", transform->getModelMatrix());
		m_engine->m_viewLayer->drawMesh(m_file);
		m_program->unbind();
	}
}

void MeshComponent::update()
{

}

void MeshComponent::destroy()
{

}
