#ifndef WindowsApplication_h
#define WindowsApplication_h

#include <Windows.h>

	#if defined(WIN32)

		#include "V_ApplicationLayer.h"


		class WindowsApplication : virtual public V_ApplicationLayer 
		{
			private:
				HDC m_hdc;
				HWND window;
				HINSTANCE m_instance;

			public:
				WindowsApplication(CGTEngine *engine) : V_ApplicationLayer(engine) {}

				bool initialise();

				void showErrorMessageAndQuit(std::string message) override;

				void update();

				void swapBuffers();

				char * loadFile(std::string file, unsigned int &length);

				bool createWindow(HINSTANCE instance, void *callBackFunc);
				HDC getDeviceContext(){ return m_hdc; }
				HWND getWindow(){ return window; }
				HINSTANCE getInstance(){ return m_instance; }
		};

	#endif // #if defines (WIN32)

#endif // WindowsApplication_h