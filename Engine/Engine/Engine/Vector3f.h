#pragma once
class Vector3f
{
public:
	float x, y, z;

	Vector3f() : x(0.0f), y(0.0f), z(0.0f){}
	Vector3f(float x) : x(x), y(x), z(x){}
	Vector3f(float x, float y, float z) : x(x), y(y), z(z){}

	float* data() { float data[] = { x, y, z }; return data; }

	const Vector3f operator += (const Vector3f &other) { x += other.x; y += other.y; z += other.z; return *this; }
	const Vector3f operator -= (const Vector3f &other) { x -= other.x; y -= other.y; z -= other.z; return *this; }
	const Vector3f operator = (const Vector3f &other) { x = other.x; y = other.y; z = other.z; return *this; }
	const Vector3f operator + (const Vector3f &other) const { return Vector3f(x + other.x, y + other.y, z + other.z); }
	const Vector3f operator - (const Vector3f &other) const { return Vector3f(x - other.x, y - other.y, z - other.z); }

	~Vector3f(){}
};

