#pragma once

#include "CGTEngine.h"
#include "V_ShaderProgram.h"

#include <map>

class ShaderManager
{
private:
	std::map<std::string, V_ShaderProgram*> m_shaderMap;
	static ShaderManager *m_instance;

	ShaderManager(){}
	ShaderManager(const ShaderManager &cpy){}
	ShaderManager operator = (const ShaderManager &cpy){}
public:
	static ShaderManager* getInstance();
	bool addShader(V_ShaderProgram *newProgram);
	bool removeShader(std::string name);
	V_ShaderProgram* getShader(std::string name);

	void setUniforms(std::string name, float val);
	void setUniforms(std::string name, int val);
	void setUniforms(std::string name, bool val);
	void setUniforms(std::string name, glm::vec2 val);
	void setUniforms(std::string name, glm::vec3 val);
	void setUniforms(std::string name, glm::vec4 val);
	void setUniforms(std::string name, glm::mat2 val);
	void setUniforms(std::string name, glm::mat3 val);
	void setUniforms(std::string name, glm::mat4 val);

	~ShaderManager(){}
};

