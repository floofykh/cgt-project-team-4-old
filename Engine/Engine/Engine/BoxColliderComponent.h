#ifndef BoxColliderComponent_h
#define BoxColliderComponent_h


#include <glm\glm.hpp>

class BoxColliderComponent {

 public:

    virtual bool isColliding(BoxColliderComponent *other);

 public:
    glm::vec3 m_centre;
    glm::vec3 m_size;
};

#endif // BoxColliderComponent_h
