// Fragment Shader � file "particles.frag"
#version 330
precision highp float; // needed only for version 1.30

in  vec3 ex_Color; 
in float height;
out vec4 out_Color; 
uniform sampler2D textureUnit0; 

void main(void)
{
	  out_Color = vec4(ex_Color, 1.0) * texture(textureUnit0, gl_PointCoord);

	  if(out_Color == vec4(0.0, 0.0, 0.0, 1.0)) //Black colour of texture set alpha to 0
	  out_Color.a = 0.0;
	 
	  out_Color.a *= (height+1); //Fade out as snow falls
}
