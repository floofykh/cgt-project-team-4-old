#version 330

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 modelViewMat;
uniform mat4 projectionMat;
uniform mat3 normalMat;
uniform vec3 eyePos;

layout (location = 0) in vec3 in_Position;
layout (location = 1) in vec3 in_Normal;
layout (location = 2) in vec3 in_TexCoord;

void main ()
{
	gl_Position = projectionMat * viewMat * modelMat * vec4(in_Position, 1.0);
}