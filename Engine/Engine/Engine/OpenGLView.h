#ifndef OpenGLView_h
#define OpenGLView_h

#include "V_ViewLayer.h"

#include <map>
#include <GL\glew.h>
#include <string>
#include <assimp\Importer.hpp>

#define checkGLErr() checkOpenGLErr(__FILE__, __LINE__);

class OpenGLView : virtual public V_ViewLayer 
{
public:
	OpenGLView(CGTEngine *engine) : V_ViewLayer(engine) {}

	void preRender() override;
	void render() override;
	void postRender() override;
	void update() override;

	bool initialise() override;

	void addMesh(bool isTransparent, const std::string file) override;

	void removeMesh(const std::string file) override;

	void drawMesh(const std::string file) override;

	~OpenGLView();

private:
	struct Mesh
	{
		GLuint vao;
		GLuint numIndices;
	};
	std::map<std::string, Mesh> m_meshes;
	Assimp::Importer *importer; 

	GLuint vbo[2];

	void assimpImport(std::string file);
	void rt3dImport(std::string file);
	friend void checkOpenGLErr(const char* file, int line);
	bool initGlew(HINSTANCE instance);
};

#endif // OpenGLView_h
