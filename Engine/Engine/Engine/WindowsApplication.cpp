#include "WindowsApplication.h"

#include "CGTEngine.h"

#include <iostream>
#include <fstream>

bool WindowsApplication::initialise() 
{
	return true;
}

bool WindowsApplication::createWindow(HINSTANCE instance, void *callBackFunc)
{
	m_instance = instance;

	WNDCLASSEX windowClass;

	memset(&windowClass, 0, sizeof(WNDCLASSEX));
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_OWNDC;
	windowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	windowClass.hIcon = LoadIcon(instance, IDI_WINLOGO);
	windowClass.hIconSm = LoadIcon(instance, IDI_WINLOGO);
	windowClass.hCursor = LoadIcon(instance, IDI_WINLOGO);
	windowClass.lpfnWndProc = (WNDPROC)callBackFunc;
	windowClass.hInstance = instance;
	windowClass.lpszClassName = "PizzaMan";
	windowClass.lpszMenuName = NULL;

	RegisterClassEx(&windowClass);

	window = CreateWindowEx(
		0,
		"PizzaMan", "Pizza Man: You Wanna Pizza Me!?",
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		0, 0,
		800, 600,
		NULL,
		NULL,
		instance,
		NULL
		);

	m_hdc = GetDC(window);

	ShowWindow(window, SW_SHOW);

	ShowWindow(window, SW_SHOW);
	UpdateWindow(window);

	return true;
}

void WindowsApplication::update()
{
	MSG message;
	while (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
	{
		if (message.message == WM_QUIT)
			exit(1);

		TranslateMessage(&message);
		DispatchMessage(&message);
	}
}

void WindowsApplication::swapBuffers()
{
	SwapBuffers(m_hdc);
}

void WindowsApplication::showErrorMessageAndQuit(std::string message)
{
	int msgboxID = MessageBox(
		NULL,
		(LPCSTR)message.c_str(),
		(LPCSTR)"Fatal Error",
		MB_OK | MB_ICONERROR
		);

	switch (msgboxID)
	{
	case IDOK:
		m_engine->close();
		break;
	}
}

char * WindowsApplication::loadFile(std::string file, unsigned int &length)
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	std::ifstream stream(file, std::ios::in | std::ios::binary | std::ios::ate);
	if (stream.is_open())
	{
		size = (int)stream.tellg(); // get location of file pointer i.e. file size
		memblock = new char[size];
		stream.seekg(0, std::ios::beg);
		stream.read(memblock, size);
		stream.close();
	}
	else 
	{
		return nullptr;
	}

	length = size;
	return memblock;
}