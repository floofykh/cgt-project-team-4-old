#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

	class Camera
	{
		private:			
			struct KylesJunk
			{
				glm::vec3 position;
				float direction;

				float nearDistance;
				float nearWidth;
				float nearHeight;

				float farDistance;
				float farWidth;
				float farHeight;
			};

			float m_fieldOfView			= 60.0f;
			float m_aspecRatio			= 4.0f/3.0f;


			glm::vec3 m_position		= glm::vec3(0.0f, 1.0f, 5.0f);
			glm::vec3 m_rotationAxis	= glm::vec3(0.0f, 1.0f, 0.0f);
			glm::mat4 m_viewMatrix		= glm::mat4(1.0f);
			float m_angle				= 0.0f;

			float m_nearDistance		= 1.0f;
			float m_farDistance			= 1.0f;

		public:
			Camera() { }
			Camera( const Camera& ) { }
			Camera operator=( const Camera& ) { }
			~Camera() { }

			void update();

			glm::mat4 getViewMatrix() { return m_viewMatrix; }

			void setPosition( glm::vec3 position ) { m_position = position; }
			glm::vec3 getPosition() { return m_position; }

			void moveForward(float distance);
			void moveRight(float distance);
			void moveUp(float distance);
			void rotateY(float angle);


			float getNearDistance() { return m_nearDistance; }
			float getFarDistance() { return m_farDistance; }

			KylesJunk gottaHave();
	};


#endif // CAMERA_H