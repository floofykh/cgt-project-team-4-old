#ifndef PizzaMan_h
#define PizzaMan_h

#include "V_GameLayer.h"
#include "Camera.h"

class PizzaMan : virtual public V_GameLayer 
{
private:
	Camera *m_camera;
public:
	PizzaMan(CGTEngine *engine) : V_GameLayer(engine){}

	bool initialise();
	void update();
};

#endif // PizzaMan_h
