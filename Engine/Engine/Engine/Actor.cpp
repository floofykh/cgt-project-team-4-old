#include "Actor.h"
#include "CGTEngine.h"
#include "TransformComponent.h"

using namespace std;
    /** 
     *  Actors represent all objects in the game, be it the player, an enemy or a shoe. 
     *  @author Kyle Hewitt
     */
unsigned int Actor::s_count = 0;


Actor::Actor(CGTEngine *engine) : Actor(engine, "" + s_count)
{
}

Actor::Actor(CGTEngine *engine, std::string name)
{
	s_count++;
	m_engine = engine;
	m_name = name;

	addComponent(new TransformComponent());

	engine->m_actors.push_back(this);
}

bool Actor::addComponent(V_ActorComponent *component)
{
	if (m_components.find(component->getName()) != m_components.end())
		return false;

	m_components[component->getName()] = component;
	component->m_actor = this;
	return true;
}

V_ActorComponent* Actor::getComponent(std::string name)
{
	return m_components[name];
}

/*template <typename T>
T* Actor::getComponent<T>()
{
	//Create iterator to traverse through map
	map<string, V_ActorComponent*>::iterator iter;

	//Iterate through map, check if the typeid matches what was declared as T, and return first one found. Return nullptr otherwise. 
	for (iter = m_components.begin(); iter != m_components.end(); ++iter)
	{
		if (typeid(iter->second) == typeid(T))
			return iter->second;
	}

	return nullptr;
}*/
