#include "CGTEngine.h"
#include "WindowsApplication.h"
#include <Windows.h>

#pragma comment( lib, "glew32.lib" )
#pragma comment( lib, "opengl32.lib" )

#ifdef _DEBUG
#pragma comment( lib, "assimpd.lib" )

// Activate the console
#pragma comment( linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"" )
#else
#pragma comment( lib, "assimp.lib" )

#endif

int pizzaMain(CGTEngine *engine)
{
	engine->initialise();

	while (engine->isRunning())
	{
		engine->update();
		engine->render();
	}

	delete engine;

	return 0;
}

#if defined(WIN32)

	static LRESULT CALLBACK mainWindowCallback(const HWND window, const UINT message, const WPARAM wParam, const LPARAM lParam)
	{
		LRESULT result = 0;
		PAINTSTRUCT paintStruct;

		switch (message)
		{
		case WM_DESTROY:
			exit(1);
			break;
		case WM_CLOSE:
			PostQuitMessage(0);
			break;
		case WM_PAINT:
			BeginPaint(window, &paintStruct);
			EndPaint(window, &paintStruct);
			break;
		default:
			result = DefWindowProc(window, message, wParam, lParam);
			break;
		}

		return result;
	}

	int _stdcall WinMain(HINSTANCE instance, HINSTANCE prevInstrance, LPSTR commandLine, int showCode)
	{
		CGTEngine *engine = new CGTEngine();
		WindowsApplication *application = new WindowsApplication(engine);
		if (!application->initialise())
		{
			application->showErrorMessageAndQuit("Failed to initialise a windows application");
			return 0;
		}
		if (!application->createWindow(instance, mainWindowCallback))
		{
			application->showErrorMessageAndQuit("Failed to initialise a window");
			return 0;
		}

		engine->m_applicationLayer = application;
		return pizzaMain(engine);
	}

#elif defined(UNIX)
	//
	// Linux stuff
	int main( int argc, char *argv[] )
	{
		return pizzaMain(engine);
	}

#endif
