#ifndef MESH_MANAGER_H
#define MESH_MANAGER_H

#include "MeshComponent.h"
#include <map>
#include <vector>
#include <string>

class CGTEngine;

class MeshManager
{
	friend class CGTEngine; //Make the CGTEngine a friend to MeshManager so it can access the private method setEngine();

private:
	struct MeshData //Struct containing data on each mesh
	{
		std::vector<MeshComponent*> components;
		float usagePerFrame; //How many times per frame the mesh is used
		float timeSinceLastUse_ms; //The number of milliseconds since the mesh was last drawn
		bool inGPUMemory; //A flag saying whether the mesh is currently in the GPUs memory.
	};

	std::map<std::string, MeshData> m_meshes;
	CGTEngine *m_engine; //Pointer to the engine to access other classes needed to function
	static MeshManager *s_instance; //Singleton instance

	MeshManager(){}
	MeshManager(const MeshManager&){}
	MeshManager operator = (const MeshManager&){}
	void setEngine(CGTEngine *engine) { m_engine = engine; }
public:
	static MeshManager *getInstance();
	MeshComponent* createMesh(std::string file);
	void drawMeshes();
	void update();
	~MeshManager();
};

#endif //MESH_MANAGER_H

