#include "TransformComponent.h"

#include <glm\gtc\matrix_transform.hpp>

TransformComponent::TransformComponent()
{
}

glm::mat4 TransformComponent::getModelMatrix() 
{ 
	glm::mat4 modelMat(1.0f); 

	modelMat = glm::translate(modelMat, m_position);
	modelMat = glm::scale(modelMat, m_scale);
	modelMat = glm::rotate(modelMat, m_rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	modelMat = glm::rotate(modelMat, m_rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	modelMat = glm::rotate(modelMat, m_rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

	return modelMat;
}

TransformComponent::~TransformComponent()
{
}
