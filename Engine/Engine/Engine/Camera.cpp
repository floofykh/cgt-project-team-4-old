#include "Camera.h"

#include <cmath>


void Camera::moveRight(float distance)
{
	m_position = glm::vec3(m_position.x + distance*cos(glm::radians(m_angle)), m_position.y, m_position.z + distance*sin(glm::radians(m_angle)));
}

void Camera::moveForward(float distance)
{
	m_position = glm::vec3(m_position.x + distance*sin(glm::radians(m_angle)), m_position.y, m_position.z - distance*cos(glm::radians(m_angle)));
}

void Camera::moveUp(float distance)
{
	m_position.y += distance;
}

void Camera::rotateY(float angle)
{
	this->m_angle += angle;
	if (this->m_angle >= 360)
		this->m_angle -= 360;
	else if (this->m_angle <= -360)
		this->m_angle += 360;
}

void Camera::update() 
{
	glm::vec3 at = glm::vec3(m_position.x + 1.0f*sin(glm::radians(m_angle)), m_position.y, m_position.z - 1.0f*cos(glm::radians(m_angle)));

	m_viewMatrix = glm::lookAt(m_position, at, m_rotationAxis);
}	


Camera::KylesJunk Camera::gottaHave() {
	KylesJunk temp;
	temp.direction = m_angle;
	temp.position = m_position;
	temp.farDistance = m_farDistance;
	temp.nearDistance = m_nearDistance;

	temp.nearHeight = tan(m_fieldOfView/2.0f) * m_nearDistance;
	temp.nearWidth = temp.nearHeight * m_aspecRatio;

	temp.farHeight = tan(m_fieldOfView/2.0f) * m_farDistance;
	temp.farWidth = temp.farHeight * m_aspecRatio;

	return temp;
}