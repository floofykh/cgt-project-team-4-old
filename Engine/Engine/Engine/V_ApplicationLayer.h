#ifndef V_ApplicationLayer_h
#define V_ApplicationLayer_h

#include <string>

class CGTEngine; //All subclasses of V_ApplicationLayer must #include "CGTEngine.h" in their .cpp file.

class V_ApplicationLayer {

 public:
	 V_ApplicationLayer(CGTEngine *engine){ m_engine = engine; };

    virtual bool initialise()  = 0;

    virtual void update()  = 0;

	virtual void swapBuffers() = 0;

	virtual void showErrorMessageAndQuit(std::string message) = 0;

	virtual char * loadFile(std::string file, unsigned int &length) = 0;

public:
    // virtual destructor for interface 
    virtual ~V_ApplicationLayer() { }

 public:

    /**
     * @element-type CGTEngine
     */
    CGTEngine *m_engine;
};

#endif // V_ApplicationLayer_h
