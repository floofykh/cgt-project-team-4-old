#ifndef CGTEngine_h
#define CGTEngine_h

#include <string>
#include <vector>
#include <map>
#include <glm/glm.hpp>

class V_GameLayer;
class V_ViewLayer;
class Actor;
class V_ApplicationLayer;
class EventManager;
class MeshManager;
class ShaderManager;
class Camera;


    /** 
     *  Main class which makes up the entire engine. Holds all the necessary objects and makes them run.
     */
class CGTEngine {

 public:


    /** 
     *  Initialises all the member variables and resources for the game.
     */
    bool initialise();

	/**
	*	Main update loop for the game. Not to be called except by main!!!
	*/
	void update();

	void render();

    /** 
     *  Saves game to the specified file. Consider using XML instead. 
     */
    bool save(std::string file);

    bool load(std::string file);

	/*Closes the game.*/
	void close(){ m_gameIsRunning = false; }

	/*Returns whether the game is running*/
	bool isRunning() { return m_gameIsRunning; }

	void setMatrix(std::string matrixName, glm::mat4 mat);
	void setMatrix(std::string name, glm::mat3 mat);

 public:
    EventManager *m_eventManager;

 private:
    
    /** 
     *  Sets the speed that the game flows at. Normal speed is 1, below one is slower, above faster. Cannot be less than 1.
     */
    float m_gameSpeed;

	bool m_gameIsRunning = true;

	std::map<std::string, glm::mat4> mat4Map;
	std::map<std::string, glm::mat3> mat3Map;

	ShaderManager* m_shaderManager = nullptr;

 public:

    /**
     * @element-type V_ApplicationLayer
     */
    V_ApplicationLayer *m_applicationLayer;

    /**
     * @element-type MeshManager
     */
	MeshManager *m_meshManager;

    /**
     * @element-type V_GameLayer
     */
    V_GameLayer *m_gameLayer;

    /**
     * @element-type V_ViewLayer
     */
    V_ViewLayer *m_viewLayer;

    /**
     * @element-type Actor
     */
    std::vector< Actor* > m_actors;

	Camera *m_camera = nullptr;
};

#endif // CGTEngine_h
