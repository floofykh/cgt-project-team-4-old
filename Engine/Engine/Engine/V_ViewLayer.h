#ifndef V_ViewLayer_h
#define V_ViewLayer_h

#include "ShaderManager.h"

#include <string>
#include <Windows.h>

class MeshComponent; //All subclasses of V_ViewLayer must #include "MeshComponent.h" in their .cpp file.
class CGTEngine; //All subclasses of V_ViewLayer must #include "CGTEngine.h" in their .cpp file.


    /** 
     *  Interface describing the functionality of any view layers that will be used by the engine. A view layer is a layer which draws to the screen.
     *  @author Kyle Hewitt
     */
class V_ViewLayer {

 public:
	 V_ViewLayer(CGTEngine *engine) { m_engine = engine; m_shaderManager = ShaderManager::getInstance(); }

    /** 
     *  Renders everything to the screen
	 *@param deltaTime seconds since last update. 
     */
	virtual void preRender() = 0;
	virtual void render() = 0;
	virtual void postRender() = 0;
	virtual void update() = 0;

	/*Initialises the object*/
    virtual bool initialise()  = 0;

	/*Adds a mesh to the ViewLayer's memory for drawing.
	*@param file the file and directory the mesh can be found, used as an identifier.
	*@param isTransparent true if the mesh's alpha value is not 1.0, false otherwise, needed for proper draw order. 
	*/
	virtual void addMesh(bool isTransparent, const std::string file) = 0;

	/*Removes a mesh from teh ViewLayer's memory
	*@param file the file and directory the mesh can be found, used as an identifier.
	*/
	virtual void removeMesh(const std::string file) = 0;

	/*Draws a mesh that is in the ViewLayer's memory
	*@param file the file and directory the mesh can be found, used as an identifier.
	*/
	virtual void drawMesh(const std::string file) = 0;

	bool setUniforms(std::string name, float val);
	bool setUniforms(std::string name, int val);
	bool setUniforms(std::string name, bool val);
	bool setUniforms(std::string name, glm::vec2 val);
	bool setUniforms(std::string name, glm::vec3 val);
	bool setUniforms(std::string name, glm::vec4 val);
	bool setUniforms(std::string name, glm::mat2 val);
	bool setUniforms(std::string name, glm::mat3 val);
	bool setUniforms(std::string name, glm::mat4 val);

public:
    // virtual destructor for interface 
    virtual ~V_ViewLayer() { }
 protected:

    /**
     * @element-type CGTEngine
     */
    CGTEngine *m_engine;

	ShaderManager *m_shaderManager;
};

#endif // V_ViewLayer_h
