#pragma once

#include "Vector4f.h"

class Matrix4f
{
private:
	float data[16] = { 
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f };
public:

	Matrix4f(){}
	Matrix4f(float x) 
	{
		for (int i = 0; i < 16; i++)
		{
				if (i % 5 == 0)
					data[i] = x;
				else
					data[i] = 0.0f;
		}
	}

	float* data() { return data; }

	void translate(const Vector4f &vector)
	{
		//Grr
	}

	const Matrix4f operator += (const Matrix4f &other) 
	{ 
		for (int i = 0; i < 16; i++)
			data[i] += other.data[i];

		return *this;
	}
	const Matrix4f operator -= (const Matrix4f &other)
	{
		for (int i = 0; i < 16; i++)
			data[i] -= other.data[i];

		return *this;
	}
	const Matrix4f operator = (const Matrix4f &other) 
	{
		for (int i = 0; i < 16; i++)
			data[i] = other.data[i];

		return *this;
	}
	const Matrix4f operator + (const Matrix4f &other) const 
	{ 
		Matrix4f newMat; 

		for (int i = 0; i < 16; i++)
			newMat.data[i] = data[i] + other.data[i];

		return newMat;
	}
	const Matrix4f operator - (const Matrix4f &other) const 
	{
		Matrix4f newMat;

		for (int i = 0; i < 16; i++)
			newMat.data[i] = data[i] - other.data[i];

		return newMat;
	}

	~Matrix4f(){}
};

