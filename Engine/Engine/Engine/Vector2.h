// Vector2.h
// 2D Vector2 class interface file
// Jonathan Livingstone
#ifndef VECTOR2_H
#define VECTOR2_h

// Standard library
#include <string>


	template <typename T>
		class Vector2
		{
			public:
				// The data memers
				// Note - they are automatically set to 0
				T x = (float) 0.0f;
				T y = (float) 0.0f;

				//============================================================================================================================================
				// Constructors
				//============================================================================================================================================

				// Default Constructor
				Vector2() : x(0), y(0) { }

				// Create and set values
				Vector2( T ix, T iy ) : x(ix), y(iy) {	}

				// Copy constructor
				Vector2( const Vector2& v ) : x(v.x), y(v.y) { }



				//============================================================================================================================================
				// Operator Overloads
				//============================================================================================================================================

				// Compare to see whether two Vectors are equal
				bool operator== ( Vector2<float> v ) {
					if ( x == v.x )
						if ( y == v.y )
							return true;

					return false;
				}
				// Compare to see whether two Vectors are equal
				bool operator== ( Vector2<int> v ) {
					if ( x == v.x )
						if ( y == v.y )
							return true;

					return false;
				}
				// Compare to see whether two Vectors are equal
				bool operator== ( Vector2<unsigned int> v ) {
					if ( x == v.x )
						if ( y == v.y )
							return true;

					return false;
				}


				// Assign one vector to another
				const Vector2& operator= ( const Vector2<float> v ) {
					(float) x = v.x;
					(float) y = v.y;
					return *this;
				}
				// Assign one vector to another
				const Vector2& operator= ( const Vector2<int> v ) {
					(float) x = (float) v.x;
					(float) y = (float) v.y;
					return *this;
				}
				// Assign one vector to another
				const Vector2& operator= ( const Vector2<unsigned int> v ) {
					(float) x = (float) v.x;
					(float) y = (float) v.y;
					return *this;
				}


				// Add a vector onto this one
				const Vector2& operator+= ( const Vector2<float> v ) {
					x += v.x;
					y += v.y;
					return *this;
				}
				// Add a vector onto this one
				const Vector2& operator+= ( const Vector2<int> v ) {
					x += v.x;
					y += v.y;
					return *this;
				}
				// Add a vector onto this one
				const Vector2& operator+= ( const Vector2<unsigned int> v ) {
					x += v.x;
					y += v.y;
					return *this;
				}


				// Subtract a vector from this one
				const Vector2& operator-= ( const Vector2<float> v ) {
					x -= v.x;
					y -= v.y;
					return *this;
				}
				// Subtract a vector from this one
				const Vector2& operator-= ( const Vector2<int> v ) {
					x -= v.x;
					y -= v.y;
					return *this;
				}
				// Subtract a vector from this one
				const Vector2& operator-= ( const Vector2<unsigned int> v ) {
					x -= v.x;
					y -= v.y;
					return *this;
				}



				// Multiply a vector by a scalar
				const Vector2& operator*= ( const float& s ) {
					x *=s ; 
					y *= s; 
					return *this;
				}


				// Divide a vector by a scalar
				const Vector2& operator/= ( const float& s ) {
					x /= s;
					y /= s;
					return *this;
				}



				// Add two vectors
				// Note - Does not change values within the vector!
				const Vector2 operator+ ( const Vector2<float> v ) const {
					return Vector2<T> (
						x + v.x,
						y + v.y
					);
				}
				// Add two vectors
				// Note - Does not change values within the vector!
				const Vector2 operator+ ( const Vector2<int> v ) const {
					return Vector2<T> (
						x + v.x,
						y + v.y
					);
				}
				// Add two vectors
				// Note - Does not change values within the vector!
				const Vector2 operator+ ( const Vector2<unsigned int> v ) const {
					return Vector2<T> (
						x + v.x,
						y + v.y
					);
				}



				// Subtract two vectors
				// Note - Does not change values within the vector!
				const Vector2 operator- ( const Vector2<float> v ) const {
					return Vector2<T> (
						x - v.x,
						y - v.y
					);
				}
				// Subtract two vectors
				// Note - Does not change values within the vector!
				const Vector2 operator- ( const Vector2<int> v ) const {
					return Vector2<T> (
						x - v.x,
						y - v.y
					);
				}
				// Subtract two vectors
				// Subtract two vectors
				// Note - Does not change values within the vector!
				const Vector2 operator- ( const Vector2<unsigned int> v ) const {
					return Vector2<T> (
						x - v.x,
						y - v.y
					);
				}



				// Multiply a vector by a scalar
				// Note - Does not change the values within the vector!
				const Vector2 operator* ( const float& s ) const {
					return Vector2<T> (
						x * s,
						y * s
					);
				}


				// Divide a vector by a scalar
				// Note - Does not change the value within the vector!
				const Vector2 operator/ (const float& s) const {
					return Vector2<T> (
						x / s,
						y / s
					);
				}
	



				//============================================================================================================================================
				// Methods
				//============================================================================================================================================

				// Return the Vector as a std::string
				// Note - The format: x + ' ' + y
				const char* asString() {
					return std::to_string(x) + ' ' + std::to_string(y);
				}

				// Return the distance between two vectors
				// As a float
				float distance( const Vector2<float> v ) const {
					return (
						sqrt(
							pow( x-v.x, 2.0f )
							+
							pow( y-v.y, 2.0f )
						)
					);
				}
				// Return the distance between two vectors
				// As a float
				float distance( const Vector2<int> v ) const {
					return (
						sqrt(
							pow( x-v.x, 2.0f )
							+
							pow( y-v.y, 2.0f )
						)
					);
				}
				// Return the distance between two vectors
				// As a float
				float distance( const Vector2<unsigned int> v ) const {
					return (
						sqrt(
							pow( x-v.x, 2.0f )
							+
							pow( y-v.y, 2.0f )
						)
					);
				}


				// Get the dot product of two vectors
				// As a float
				const float dot( const Vector2<float> v ) const {
					return ( 
						( x * v.x )
						+
						( y * v.y )
					);
				}
				// Get the dot product of two vectors
				// As a float
				const float dot( const Vector2<int> v ) const {
					return ( 
						( x * v.x )
						+
						( y * v.y )
					);
				}
				// Get the dot product of two vectors
				// As a float
				const float dot( const Vector2<unsigned int> v ) const {
					return ( 
						( x * v.x )
						+
						( y * v.y )
					);
				}



				// Get the length of this vector
				// As a float
				const float magnitude() const {
					return std::sqrt (
						pow( x, 2.0f )
						+
						pow( y, 2.0f )
					);
				}


				// Return a unit Vector2 in same direction as this Vector2
				const Vector2 unit() const {
					return Vector2<T> (
						x / magnitude();,
						y / magnitude();
					);
				}
	

				// Make this a unit vector
				// Same direction, length of 1
				void normalize() {
					sqrt( pow(x, 2.0f) + pow(y, 2.0f) );
				}


		};

	//=================================================================================================================================================
	// Define some standard vector types
	//=================================================================================================================================================
		
	// Both points in the Vector are integers
	// This is a typedef for sgl::Vector2<int>
	typedef Vector2<int>          Vector2i;

	// Both points in the Vector are unsigned integers
	// This is a typedef for sgl::Vector<unsigned int>
	typedef Vector2<unsigned int> Vector2u;

	// Both points in the Vector are floats
	// This is a typedef for sgl::Vector2<float>
	typedef Vector2<float>        Vector2f;


#endif // VECTOR2_H