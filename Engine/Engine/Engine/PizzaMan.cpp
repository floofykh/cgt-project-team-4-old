#include "PizzaMan.h"

#include "CGTEngine.h"
#include "Actor.h"
#include "MeshManager.h"
#include "ShaderManager.h"
#include "GLShaderProgram.h"
#include "TransformComponent.h"

Actor *theCube;

bool PizzaMan::initialise()
{
	MeshManager *meshManager = MeshManager::getInstance();
	ShaderManager *shaderManager = ShaderManager::getInstance();

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			Actor *cube = new Actor(m_engine);
			MeshComponent *mesh = meshManager->createMesh("Models/cube.obj");
			mesh->setShaderProgram(shaderManager->getShader("Phong"));
			cube->addComponent(mesh);
			TransformComponent *transform = (TransformComponent*)(cube->getComponent("Transform"));
			transform->setPosition(glm::vec3(-50 + i * 10, -50.0f, -50 + j * 10));
			transform->setScale(glm::vec3(50.1f, 50.1f, 50.1f));

			m_engine->m_actors.push_back(cube);
		}
	}
	theCube = new Actor(m_engine);
	MeshComponent *mesh = meshManager->createMesh("Models/cube.obj");
	mesh->setShaderProgram(shaderManager->getShader("Phong"));
	theCube->addComponent(mesh);
	TransformComponent *transform = (TransformComponent*)(theCube->getComponent("Transform"));
	transform->setPosition(glm::vec3(100.0f, 15.0f, -50.0f));
	transform->setScale(glm::vec3(50.0f, 50.0f, 50.0f));
	m_engine->m_actors.push_back(theCube);

	m_camera = m_engine->m_camera;
	m_camera->setPosition(glm::vec3(-60.0f, 23.0f, 72.0f));

	return true;
}

void PizzaMan::update()
{
}