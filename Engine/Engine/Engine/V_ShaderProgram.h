#pragma once

#include <glm\glm.hpp>
#include "CGTEngine.h"

class V_ShaderProgram
{
protected:
	CGTEngine *m_engine = nullptr;
	std::string m_name;
public:
	V_ShaderProgram(CGTEngine *engine, std::string name) : m_engine(engine), m_name(name){}

	virtual bool intitialise() = 0;

	virtual void setShaderUniform(std::string name, float val) = 0;
	virtual void setShaderUniform(std::string name, int val) = 0;
	virtual void setShaderUniform(std::string name, bool val) = 0;
	virtual void setShaderUniform(std::string name, glm::vec2 val) = 0;
	virtual void setShaderUniform(std::string name, glm::vec3 val) = 0;
	virtual void setShaderUniform(std::string name, glm::vec4 val) = 0;
	virtual void setShaderUniform(std::string name, glm::mat2 val) = 0;
	virtual void setShaderUniform(std::string name, glm::mat3 val) = 0;
	virtual void setShaderUniform(std::string name, glm::mat4 val) = 0;

	std::string getName(){ return m_name; }

	virtual void bind() = 0;
	virtual void unbind() = 0;

	~V_ShaderProgram(){}
};

