#pragma once
#include "V_ShaderProgram.h"
#include <string>
#include <GL\glew.h>
#include <map>

class GLShaderProgram :
	public V_ShaderProgram
{
private:
	enum
	{
		VERTEX_SHADER,
		TESSELLATION_CONTROL_SHADER,
		TESSELLATION_EVALUATION_SHADER,
		GEOMETRY_SHADER,
		FRAGMENT_SHADER
	};

	std::string m_shaderFiles[5];
	GLint m_shaders[5];
	GLuint m_program;
	std::map<std::string, GLuint> m_uniformLocations;

	void createShader(std::string file, int type);
	bool compile(GLuint id);
	void printError(const GLint id);

	GLuint getUniformLocation(std::string);

public:
	GLShaderProgram(CGTEngine *engine, std::string vertexShader, std::string fragmentShader, std::string name);
	GLShaderProgram(CGTEngine *engine, std::string vertexShader, std::string geometryShader, std::string fragmentShader, std::string name);
	GLShaderProgram(CGTEngine *engine, std::string vertexShader, std::string tesControlShader, std::string tesEvalShader,
		std::string geometryShader, std::string fragmentShader, std::string name);

	bool intitialise() override;

	void setShaderUniform(std::string name, float val) override;
	void setShaderUniform(std::string name, int val) override;
	void setShaderUniform(std::string name, bool val) override;
	void setShaderUniform(std::string name, glm::vec2 val) override;
	void setShaderUniform(std::string name, glm::vec3 val) override;
	void setShaderUniform(std::string name, glm::vec4 val) override;
	void setShaderUniform(std::string name, glm::mat2 val) override;
	void setShaderUniform(std::string name, glm::mat3 val) override;
	void setShaderUniform(std::string name, glm::mat4 val) override; 
	
	void bind()override;
	void unbind()override;

	~GLShaderProgram();
};

