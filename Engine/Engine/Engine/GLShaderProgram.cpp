#include "GLShaderProgram.h"
#include "V_ApplicationLayer.h"
#include "OpenGLView.h"
#include <glm\gtc\type_ptr.hpp>

#include <iostream>

GLShaderProgram::GLShaderProgram(CGTEngine *engine, std::string vertexShader, std::string fragmentShader, std::string name) :
GLShaderProgram(engine, vertexShader, "", "", "", fragmentShader, name)
{
}

GLShaderProgram::GLShaderProgram(CGTEngine *engine, std::string vertexShader, std::string geometryShader, std::string fragmentShader, std::string name) :
GLShaderProgram(engine, vertexShader, "", "", geometryShader, fragmentShader, name)
{
}

GLShaderProgram::GLShaderProgram(CGTEngine *engine, std::string vertexShader, std::string tesControlShader, std::string tesEvalShader,
	std::string geometryShader, std::string fragmentShader, std::string name) : V_ShaderProgram(engine, name)
{
	m_shaderFiles[VERTEX_SHADER] = vertexShader;
	m_shaderFiles[TESSELLATION_CONTROL_SHADER] = tesControlShader;
	m_shaderFiles[TESSELLATION_EVALUATION_SHADER] = tesEvalShader;
	m_shaderFiles[GEOMETRY_SHADER] = geometryShader;
	m_shaderFiles[FRAGMENT_SHADER] = fragmentShader;
}

bool GLShaderProgram::intitialise()
{
	m_program = glCreateProgram();
	checkGLErr();

	for (int i = 0; i < 5; i++)
	{
		createShader(m_shaderFiles[i], i);

		if (m_shaders[i] != -1)
			glAttachShader(m_program, m_shaders[i]);
		checkGLErr();
	}

	glLinkProgram(m_program);
	checkGLErr();
	glUseProgram(m_program);
	checkGLErr();

	glUseProgram(0);

	return true;
}

void GLShaderProgram::createShader(std::string file, int type)
{
	GLenum glShaderEnum;
	switch (type)
	{
	case VERTEX_SHADER:
		glShaderEnum = GL_VERTEX_SHADER;
		break;
	case TESSELLATION_CONTROL_SHADER:
		glShaderEnum = GL_TESS_CONTROL_SHADER;
		break;
	case TESSELLATION_EVALUATION_SHADER:
		glShaderEnum = GL_TESS_EVALUATION_SHADER;
		break;
	case GEOMETRY_SHADER:
		glShaderEnum = GL_GEOMETRY_SHADER;
		break;
	case FRAGMENT_SHADER:
		glShaderEnum = GL_FRAGMENT_SHADER;
		break;
	};

	if (file.size() == 0)
	{
		m_shaders[type] = -1;
		return;
	}

	GLuint id = glCreateShader(glShaderEnum);
	checkGLErr();

	char * source;
	unsigned int length = 0;

	source = m_engine->m_applicationLayer->loadFile(file, length);

	if (source == NULL)
	{
		m_shaders[type] = -1;
		return;
	}

	glShaderSource(id, 1, &source, (GLint*)&length);
	checkGLErr();

	if (compile(id))
		m_shaders[type] = id;
	else
		m_shaders[type] = -1;
}

bool GLShaderProgram::compile(GLuint shaderID)
{
	GLint compiled;
	bool successful = true;

	glCompileShader(shaderID);
	checkGLErr();
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compiled);
	checkGLErr();
	if (!compiled)
	{
		std::cout << m_name << " shader not compiled" << std::endl;
		printError(shaderID);
		successful = false;
	}

	return successful;
}

void GLShaderProgram::printError(const GLint id)
{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(id))
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);
	checkGLErr();

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(id))
			glGetProgramInfoLog(id, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(id, maxLength, &logLength, logMessage);
		std::cout << "Shader Info Log:" << std::endl << logMessage << std::endl;
		delete[] logMessage;
		checkGLErr();
	}
	// should additionally check for OpenGL errors here
}

void GLShaderProgram::setShaderUniform(std::string name, float val)
{
	bind();
	glUniform1f(getUniformLocation(name), val);
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, int val)
{
	bind();
	glUniform1i(getUniformLocation(name), val);
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, bool val)
{
	bind();
	glUniform1i(getUniformLocation(name), val);
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, glm::vec2 val)
{
	bind();
	glUniform2fv(getUniformLocation(name), 1, glm::value_ptr(val));
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, glm::vec3 val)
{
	bind();
	glUniform3fv(getUniformLocation(name), 1, glm::value_ptr(val));
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, glm::vec4 val)
{
	bind();
	glUniform4fv(getUniformLocation(name), 1, glm::value_ptr(val));
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, glm::mat2 val)
{
	bind();
	glUniformMatrix2fv(getUniformLocation(name), 1, GL_FALSE, glm::value_ptr(val));
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, glm::mat3 val)
{
	bind();
	glUniformMatrix3fv(getUniformLocation(name), 1, GL_FALSE, glm::value_ptr(val));
	unbind();
	checkGLErr();
}
void GLShaderProgram::setShaderUniform(std::string name, glm::mat4 val)
{
	bind();
	glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, glm::value_ptr(val));
	unbind();
	checkGLErr();
}

GLuint GLShaderProgram::getUniformLocation(std::string name)
{
	if (m_uniformLocations.find(name) == m_uniformLocations.end())
	{
		GLuint location = glGetUniformLocation(m_program, name.c_str());
		checkGLErr();
		if (location == -1)
			std::cout << "Could not find uniform location " << name << " in shader " << m_name << std::endl;

		m_uniformLocations[name] = location;
	}
	
	return m_uniformLocations[name];
}

void GLShaderProgram::bind() 
{ 
	glUseProgram(m_program);
	checkGLErr();
}
void GLShaderProgram::unbind() 
{ 
	glUseProgram(0); 
}

GLShaderProgram::~GLShaderProgram()
{
}
