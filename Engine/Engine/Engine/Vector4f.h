#pragma once
class Vector4f
{
public:
	float x, y, z, w;

	Vector4f() : Vector4f(0.0f) {}
	Vector4f(float x) : x(x), y(x), z(x), w(x) {}
	Vector4f(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

	float* data() { float data[] = { x, y, z, w }; return data; }

	const Vector4f operator += (const Vector4f &other) { x += other.x; y += other.y; z += other.z; w += other.w; return *this; }
	const Vector4f operator -= (const Vector4f &other) { x -= other.x; y -= other.y; z -= other.z; w -= other.w; return *this; }
	const Vector4f operator = (const Vector4f &other) { x = other.x; y = other.y; z = other.z; w = other.w; return *this; }
	const Vector4f operator + (const Vector4f &other) const { return Vector4f(x + other.x, y + other.y, z + other.z, w + other.w); }
	const Vector4f operator - (const Vector4f &other) const { return Vector4f(x - other.x, y - other.y, z - other.z, w - other.w); }

	~Vector4f(){}
};

