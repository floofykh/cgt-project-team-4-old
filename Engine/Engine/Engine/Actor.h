#ifndef Actor_h
#define Actor_h

#include "V_ActorComponent.h"

#include <map>
#include <string>

class CGTEngine;


    /** 
     *  Actors represent all objects in the game, be it the player, an enemy or a shoe. 
     *  @author Kyle Hewitt booo
     */
class Actor : virtual public V_ActorComponent {


 private:
	 static unsigned int s_count;
	 std::string m_name;
    /**
     * @element-type V_ActorComponent
     */
    std::map<std::string,  V_ActorComponent* > m_components;

public:

	Actor(CGTEngine *engine);
	Actor(CGTEngine *engine, std::string name);

	bool intialise(){ return true; }

	void draw(){}

	void update(){}

	std::string getName(){ return m_name; }

	bool addComponent(V_ActorComponent *component);

	//Gets a component by the name provided when it was added.
	V_ActorComponent* getComponent(std::string name);
	//Gets a component by the type provided to T. 
	//template <typename T> T* getComponent<T>();

	virtual void destroy(){}
};

#endif // Actor_h
