#ifndef EventManager_h
#define EventManager_h

#include <vector>


class Event;

class EventManager {

 public:

    virtual void addEvent(const Event *event);

    virtual bool removeEvent(const Event *event);

 public:

    /** 
     *  This should be a priority queue. Cannot make a template class in ArgoUML however. 
     * @element-type Event
     */
    std::vector< Event* > m_events;
};

#endif // EventManager_h
