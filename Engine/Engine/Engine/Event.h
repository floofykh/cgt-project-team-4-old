#ifndef Event_h
#define Event_h

class EventManager;

class Event {

 public:
    int priority;

 public:

	/** 
     *  This should be a priority queue. Cannot make a template class in ArgoUML however. 
     */
    EventManager *m_eventManager;
};

#endif // Event_h
