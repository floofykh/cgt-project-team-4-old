#pragma once
#include "V_ActorComponent.h"

#include <glm\glm.hpp>

class TransformComponent :
	public V_ActorComponent
{
private:
	glm::vec3 m_position, m_rotation, m_scale; //Should use our own Maths classes for platform independence. 
public:
	TransformComponent();

	bool intialise(){ return true; }

	void draw(){}

	void update(){}

	std::string getName(){ return "Transform"; }

	void setPosition(glm::vec3 position) { m_position = position; }
	void translate(glm::vec3 position) { m_position += position; }
	void setRotation(glm::vec3 rotation) { m_rotation = rotation; }
	void rotate(glm::vec3 rotation) { m_rotation += rotation; }
	void setScale(glm::vec3 scale) { m_scale = scale; }
	void scale(glm::vec3 scale) { m_scale += scale; }
	glm::mat4 getModelMatrix();

	void destroy(){}
	~TransformComponent();
};

